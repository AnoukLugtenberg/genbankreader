/*
 * Copyright (c) 2016 Anouk Lugtenberg
 * All rights reserved
 * www.bioinf.nl/~ahclugtenberg
 */
package nl.bioinf.ahclugtenberg.GenBankReaderMaven;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is responsible for setting the values which are needed to process the file.
 *
 * @author ahclugtenberg
 */
public class OptionsProvider {
    private final File genBankFile;
    private String data;
    private String definition = "";
    private String accession = "";
    private String organism = "";
    private int sequenceLength = 0;
    private int geneAmount;
    private int geneAmountForwardStrand;
    private int amountCDS;
    private String dnaSeq;
    private Set<String> startStop;
    private String proteinSeq;

    /**
     * @param genBankFile the genbank file specified by the user
     */
    public OptionsProvider(final File genBankFile) {
        this.genBankFile = genBankFile;
    }

    /**
     * This is used if user gives option --summary.
     */
    public void initializeSummary() {
        openFile();
        setDefinition();
        setAccession();
        setOrganism();
        setSequenceLength();
        setGeneAmount();
        setGeneAmountForwardStrand();
        setAmountCDS();
    }

    /**
     * This is used if user gives option --fetch_gene.
     * @param gene the gene name pattern to be used specified by the user
     */
    public void initializeFetchGene(final String gene) {
        openFile();
        setDnaSeq();
        setGeneStartStop(gene);
        getGeneStartStop();
    }
    /**
     * This is used if user gives option fetch_cds.
     * @param gene the gene name pattern to be used specified by the user
     */
    public void initializeCDS(final String gene) {
        openFile();
        setProteinSeq(gene);
    }

    /**
     * Opens the file.
     */
    private void openFile() {
        try {
            FileReader fr = new FileReader(this.genBankFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                this.data += line + "\n";
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(OptionsProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * sets the definition to be used.
     */
    private void setDefinition() {
        Pattern pattern = Pattern.compile(" *DEFINITION  *([^.]*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.definition = matcher.group(1);
        }
    }

    /**
     * serves the definition to be used.
     * @return definition the definition
     */
    public String getDefinition() {
        return this.definition;
    }

    /**
     * sets the accession to be used.
     */
    private void setAccession() {
        Pattern pattern = Pattern.compile("ACCESSION *(.*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.accession = matcher.group(1);
        }
    }
    /**
     * serves the accession to be used.
     * @return accession the accession
     */
    public String getAccession() {
        return this.accession;
    }
    /**
     * sets the organism to be used.
     */
    private void setOrganism() {
        Pattern pattern = Pattern.compile(" *ORGANISM *(.*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.organism = matcher.group(1);
        }
    }
    /**
     * serves the organism to be used.
     * @return organism the organism
     */
    public String getOrganism() {
        return this.organism;
    }
    /**
     * sets the sequence length to be used.
     */
    private void setSequenceLength() {
        Pattern pattern = Pattern.compile(" *LOCUS.* ((\\d+) bp)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.sequenceLength = Integer.parseInt(matcher.group(2));
        }
    }
    /**
     * serves the sequence length.
     * @return sequenceLength the length of the sequence
     */
    public int getSequenceLength() {
        return this.sequenceLength;
    }
    /**
     * sets the gene amount to be used.
     */
    private void setGeneAmount() {
        Pattern pattern = Pattern.compile(" {2,}gene {2,}");
        Matcher matcher = pattern.matcher(this.data);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        this.geneAmount = i;
    }
    /**
     * serves the gene amount.
     * @return geneAmount the amount of genes
     */
    public int getGeneAmount() {
        return this.geneAmount;
    }
    /**
     * sets the gene amount in forward strand to be used.
     */
    private void setGeneAmountForwardStrand() {
        Pattern pattern = Pattern.compile(" {2,}gene {2,}complement");
        Matcher matcher = pattern.matcher(this.data);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        this.geneAmountForwardStrand = i;
    }
    /**
     * serves the gene amount of forward strand.
     * @return geneAmountForwardStrand the amount of the gene in the forward strand
     */
    public int getGeneAmountForwardStrand() {
        return this.geneAmountForwardStrand;
    }
    /**
     * sets the amount of CDS to be used.
     */
    private void setAmountCDS() {
        Pattern pattern = Pattern.compile(" {2,}CDS {2,}");
        Matcher matcher = pattern.matcher(this.data);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        this.amountCDS = i;
    }
    /**
     * serves the amount of CDS.
     * @return amountCDS the amount of CDS
     */
    public int getAmountCDS() {
        return this.amountCDS;
    }
    /**
     * sets dna seq to be used.
     */
    public void setDnaSeq() {
        Pattern pattern = Pattern.compile(" *ORIGIN *\\n *((\\d* * \\w*\\n*)*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            String seq = matcher.group(1).replaceAll("[ \\d\\n]", "");
            this.dnaSeq = seq;
        }
    }
    /**
     * serves the dna sequence.
     * @return dnaSeq the dna sequence
     */
    public String getDnaSeq() {
        return this.dnaSeq;
    }
    /**
     * sets the start and stop place from the given gene.
     * @param gene gene specified by user
     */
    public void setGeneStartStop(final String gene) {
        //*Misschien nog iets met complement doen..?*//
        Set<String> ss = new LinkedHashSet<>();
        Pattern pattern = Pattern.compile("\\w*? *\\(?(\\d*\\.\\.\\d*)\\)?\\n * \\/(gene=|locus_tag=)\\\""
                + gene + "\\\"");
        Matcher matcher = pattern.matcher(this.data);
        while (matcher.find()) {
            ss.add(matcher.group(1));
        }
        this.startStop = ss;
    }
    /**
     * serves the start and stop position of genes.
     * @return startStop the start and stop position of the gene
     */
    public Set<String> getGeneStartStop() {
        return this.startStop;
    }
    /**
     * sets the protein sequence from the given gene.
     * @param gene gene to match specified by the user
     */
    public void setProteinSeq(final String gene) {
        Pattern pattern = Pattern.compile("\\/product=\\\"" + gene + "\\\".*\\/translation=\"(([A-Z]*\\n* *)*)\"",
                Pattern.DOTALL);
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.proteinSeq = matcher.group(1).trim().replaceAll(" |\n", "");
        }
    }
    /**
     * serves the protein sequence.
     * @return proteinSeq the protein sequence
     */
    public String getProteinSeq() {
        return this.proteinSeq;
    }
}
