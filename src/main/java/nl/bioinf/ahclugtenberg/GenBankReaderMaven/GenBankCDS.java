/*
 * Copyright (c) 2016 Anouk Lugtenberg
 * All rights reserved
 * www.bioinf.nl/~ahclugtenberg
 */
package nl.bioinf.ahclugtenberg.GenBankReaderMaven;

import java.io.File;

/**
 * prints the information to the screen if the user givese option --fetch_cds.
 * @author ahclugtenberg
 */
public class GenBankCDS {

    private final File genBankFile;
    private final String productName;
    private OptionsProvider data;

    /**
     * @param genBankFile the genbankfile specified by the user
     * @param productName the pattern to be matched, specified by the user
     */
    public GenBankCDS(final File genBankFile, final String productName) {
        this.genBankFile = genBankFile;
        this.productName = productName;
    }
    /**
     * initializes the process of returning the protein seq which matches with the pattern.
     * the user has specified
     */
    public void initializeCDS() {
        getCDSInformation();
        printProteinSeq();
    }
    /**
     * gets cds information from genbank file.
     */
    private void getCDSInformation() {
        this.data = new OptionsProvider(this.genBankFile);
        this.data.initializeCDS(this.productName);
    }
    /**
     * prints the info about the protein seq to the screen.
     */
    private void printProteinSeq() {
        String parsedStr = this.data.getProteinSeq().replaceAll("(.{80})", "$1\n");
        System.out.println(">CDS " + this.productName + " sequence\n" + parsedStr);
    }
}
