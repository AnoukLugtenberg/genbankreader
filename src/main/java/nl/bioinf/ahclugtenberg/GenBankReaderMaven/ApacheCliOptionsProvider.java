/*
 * Copyright (c) 2016 Anouk Lugtenberg
 * All rights reserved
 * www.bioinf.nl/~ahclugtenberg
 */
package nl.bioinf.ahclugtenberg.GenBankReaderMaven;
import java.io.File;
import java.io.IOException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

/**
 * This class implements how the CLI options are handled.
 */
public class ApacheCliOptionsProvider {
    private static final String HELP = "help";
    private static final String INFILE = "infile";
    private static final String SUMMARY = "summary";
    private static final String FETCH_GENE = "fetch_gene";
    private static final String FETCH_CDS = "fetch_cds";
    private Options options;
    private CommandLine commandLine;
    private final String[] args;

    /**
     * Processing of user arguments is initiliazid.
     * @param args The arguments given by the user via command line
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.args = args;
        initialize();
    }

    /**
     * Options initialize and processed.
     */
    private void initialize() {
        buildOptions();
        try {
            processCommandLine();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Builds the options.
     */
    private void buildOptions() {
        //* create Options object *//
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option summaryOption = new Option("s", SUMMARY, false, "Returns a textual summary of the parsed file: parsed "
                + "file and length of the sequence, for genes: count and forward/reverse balance and for CS features: "
                + "count only");
        Option fetchGeneOption = new Option("fg", FETCH_GENE, true, "Returns nucleotide sequences of the genes "
                + "that match the gene name pattern in Fasta format. --fetch_gene <PATTERN>");
        Option fileOption = new Option("f", INFILE, true, "The GenBank file to be parsed");
        Option fetchCDSOption = new Option("fc", FETCH_CDS, true, "Returns protein sequence of the genes that match "
                + "the gene name pattern");

        //*Adds options*//
        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(summaryOption);
        options.addOption(fetchGeneOption);
        options.addOption(fetchCDSOption);
    }
    /**
     * Processes the command line arguments.
     * @throws IOException if there is an issue with the file provided by the user
     * @throws ParseException is there is an issue with parsing the command line options
     */
    private void processCommandLine() throws IOException {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.args);
            if (commandLine.hasOption(HELP)) {
                printHelp();
            }
            String inputFile = commandLine.getOptionValue(INFILE);
            if (inputFile == null) {
                throw new IOException("No input file specified");
            } else {
                //*Checks if file exists and throws exception if not*//
                File file = new File(inputFile);
                if (!file.exists()) {
                    throw new IOException("input file [" + inputFile + "] not found");
                }
                //*Checks if file is a genbank file*//
                String ext = inputFile.substring(inputFile.lastIndexOf(".") + 1, inputFile.length());
                String gb = "gb";
                if (!ext.equals(gb)) {
                    throw new IOException("Provide a genbank file");
                }

                if (commandLine.hasOption(SUMMARY)) {
                    GenBankSummary summary = new GenBankSummary(file);
                    summary.printSummary();
                }

                String geneName = commandLine.getOptionValue(FETCH_GENE);
                if (commandLine.hasOption(FETCH_GENE)) {
                    GenBankGene fetchGene = new GenBankGene(file, geneName);
                    fetchGene.initialize();
                }
                
                String productName = commandLine.getOptionValue(FETCH_CDS);
                if (commandLine.hasOption(FETCH_CDS)) {
                    GenBankCDS fetchCDS = new GenBankCDS(file, productName);
                    fetchCDS.initializeCDS();
                }
            }
            } catch (ParseException e) {
            throw new IllegalStateException(e);
        }
        }
        /**
         * Prints the help.
         */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("A parser for files in GenBank format", options);
    }
}
