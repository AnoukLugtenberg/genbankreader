/*
 * Copyright (c) 2016 Anouk Lugtenberg
 * All rights reserved
 * www.bioinf.nl/~ahclugtenberg
 */

package nl.bioinf.ahclugtenberg.GenBankReaderMaven;

import java.util.Arrays;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 *
 * @author anouk
 */

public final class GenBankReader {
    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private GenBankReader() {
    }
     /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);

        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
        }
    }
}

