/*
 * Copyright (c) 2016 Anouk Lugtenberg
 * All rights reserved
 * www.bioinf.nl/~ahclugtenberg
 */
package nl.bioinf.ahclugtenberg.GenBankReaderMaven;
import java.io.File;
import java.util.ArrayList;
/**
 * prints the information to the screen if user gives option -fetch_gene.
 * @author ahclugtenberg
 */
public class GenBankGene {
    private final File genBankFile;
    private final String geneName;
    private OptionsProvider data;
    private String dnaSeq;

    /**
     * @param genBankFile the genbank file specified by the user
     * @param geneName the genename to be used to match the pattern in the genbank file
     */
    public GenBankGene(final File genBankFile, final String geneName) {
        this.genBankFile = genBankFile;
        this.geneName = geneName;
    }

    /**
     * Initializes the process for parsing genbankfile and returning the nucleotide sequences of the genes that match the gene name pattern.
     */
    public void initialize() {
        getGeneInformation();
        getDnaSeq();
    }

    /**
     * gets gene information from genbank file.
     */
    private void getGeneInformation() {
        this.data = new OptionsProvider(this.genBankFile);
        this.data.initializeFetchGene(this.geneName);
    }

    /**
     * extracts the dna sequence matching the name pattern from the genbank file.
     */
    private void getDnaSeq() {
        ArrayList<String> dna = new ArrayList<>();
        for (String i : this.data.getGeneStartStop()) {
            String[] parts = i.split("\\.\\.");
            //Convert string to int
            int start = Integer.parseInt(parts[0]) - 1;
            int stop = Integer.parseInt(parts[1]);
            this.dnaSeq = this.data.getDnaSeq().substring(start, stop);
            dna.add(dnaSeq);
        }
        for (String i : dna) {
            String parsedStr = i.replaceAll("(.{80})", "$1\n");
            System.out.println(">gene " + this.geneName + " sequence\n" + parsedStr);
        }
    }
}
