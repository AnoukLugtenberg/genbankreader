/*
 * Copyright (c) 2016 Anouk Lugtenberg
 * All rights reserved
 * www.bioinf.nl/~ahclugtenberg
 */
package nl.bioinf.ahclugtenberg.GenBankReaderMaven;

import java.io.File;

/**
 *This class implements how the output is handled if user gives option --summary.
 * @author ahclugtenberg
 */
public class GenBankSummary {
    private final File genBankFile;
    private OptionsProvider data;
    /**
     * @param genBankFile genbank file specified by the user
     */
    public GenBankSummary(final File genBankFile) {
        this.genBankFile = genBankFile;
        getSummaryInfo();
    }

    /**
     * extracts summary info about genbank file.
     */
    private void getSummaryInfo() {
        this.data = new OptionsProvider(this.genBankFile);
        this.data.initializeSummary();
    }
    /**
     * prints the summary of the genbank file to the screen.
     */
    public void printSummary() {
        System.out.println("file              " + this.genBankFile.getName());
        System.out.println("organism          " + this.data.getOrganism());
        System.out.println("accession         " + this.data.getAccession());
        System.out.println("sequence length   " + this.data.getSequenceLength() + "bp");
        System.out.println("number of genes   " + this.data.getGeneAmount());
        try {
            System.out.println("gene F/R balance  "
                    + ((double) (this.data.getGeneAmountForwardStrand()) / (double) (this.data.getGeneAmount())));
        } catch (ArithmeticException ae) {
            System.out.println("ArithmeticException by " + ae);
        }
        System.out.println("number of CDSs    " + this.data.getAmountCDS());
    }
}