#READ ME

This readme contains info to get started with the genbankreader

##What does this genbankreader do?
The genbankreader is a program used to process GenBank files via command line arguments. Normally a GenBank file features a lot of features. From all these features this parser only extracts the following fields:

* Definition
* Accession
* Source/organism
* CDS/coordinates
* CDS/product
* CDS/protein_id
* CDS/translation
* Gene/coordinates
* Gene/gene or Gene/locus_tag
* Origin (nucleotide sequence)

##How do I get set up?

This is a maven project, so it's build with all it's dependencies.
To get the program running, execute the following command line argument:

```java -jar target/GenBankReaderMaven-1.0 --infile <INFILE>```

Current functionality:

- -h/--help shows informative help/usage information
```java -jar target/GenBankReaderMaven-1.0 --help```
- -f/--infile the location of the GenBank file
- -s/--summary returns a textual summary of the parsed file
```java -jar target/GenBankReaderMaven-1.0 --infile <INFILE> --summary
- -fg/--fetch_gene returns nucleotide sequences of the genes that match the gene name pattern, in Fasta format
```java -jar target/GenBankReaderMaven-1.0 --infile <INFILE> --fetch_gene <GENE NAME (-PATTERN)>```
- -fc/--fetch_cds returns the amino acid sequences of the CDSs that match the product name pattern, in Fasta format
```java -jar target/GenBankReaderMaven-1.0 --infile <INFILE> --fetch_cds <PRODUCT NAME (-PATTERN)>```

For more information about this program you can check out the javadoc found in target/site/apidocs.

There's an example GenBank file found in the download section of this repository. This can be used to test the program and its functionality.

###Contact
Any questions about the program can be forwarded to Anouk Lugtenberg.
E-mail: ahclugtenberg@gmail.com